.SUFFIXES : .cc .o

SRCS = main.cc
OBJECTS = $(SRCS:%.cc=%.o)
TARGET = $(SRCS:%.cc=%)

CC = g++
CPPFLAGS = -g -c

all : $(TARGET)

$(TARGET) : $(OBJECTS)
	$(CC) -o $@ $@.o

%.o : %.cc
	$(CC) $(CPPFLAGS) $<

clean :
	rm -rf $(OBJECTS) $(TARGET)